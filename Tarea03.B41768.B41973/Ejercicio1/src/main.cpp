#include <iostream>
#include <time.h>
#include "cacheLRU.hpp"
#include "cachemapdirec.hpp"
using namespace std;


int main(){
    clock_t tStart = clock(), tEnd;
    CacheLRU* cacheL1_1 = new CacheLRU(19, 8, 5);
    CacheLRU* cacheL1_2 = new CacheLRU(19, 8, 5);
    CacheMapDirec* cacheL2 = new CacheMapDirec(15, 12, 5);
    
    char num;
    string dato;
    int LS;
    int ignorar;
    int contador_entradas = 1; // Contador de la cantidad de instrucciones totales.
    int entradas_P1 = 0; // Contador de la cantidad de instrucciones del procesador 1.
    int entradas_P2 = 0; // Contador de la cantidad de instrucciones del procesador 2.
    cin >> num;

    while(!cin.eof() & num!='\n'){
    
    cin >> LS;
    cin >> dato;
    cin >> ignorar;

    //++++++++++++++++++++++++++++++++++++++++++++

    int *pos; 
    int MESI_otro_L1;
    int nuevo_estado;
    if (contador_entradas%4 == 0) { //SOLICITUD PARA L1_1
        entradas_P1 =  entradas_P1 + 1;

        /*Se llama al metodo solicitud con el dato. Este devuelve el index en pos[0],
        ,el tag en pos[3], y si dio un hit 0 o un miss 1 en pos[2], y la via en caso de
        haber un hit en pos[1] */
        pos = cacheL1_1->solicitud(dato);

        // Si hay un miss en L1, se llama a L2 con el dato.
        if (pos[2]==0) {
            cacheL2->solicitud(dato);
        }

        /*Se llama al metodo obtener_estadoMESI con el index y el tag. Este retorna un 4 si 
        el dato no esta en ninguna via del index, o el estado MESI en caso de si estar.*/
        MESI_otro_L1 = cacheL1_2->obtener_estadoMESI(pos[0], pos[3]);

        /*El metodo actualiza el estado MESI del cache actual, a partir del estado del otro
        cache, y si se dio un load o un store. También retorna el nuevo estado MESI del otro cache.*/
        nuevo_estado = cacheL1_1->calc_estadoMESI(LS, pos, MESI_otro_L1); 

        /*Se actualiza en estado MESI del otro cache con el retorno del metodo anterior.*/
        cacheL1_2->actualizar_estadoMESI(nuevo_estado, pos);
    }
    else {  //SOLICITUD PARA L1_2
        entradas_P2 =  entradas_P2 + 1;
        pos = cacheL1_2->solicitud(dato);

        // Si hay un miss en L1, se llama a L2.
        if (pos[2]==0) {
            cacheL2->solicitud(dato);
        }

        MESI_otro_L1 = cacheL1_1->obtener_estadoMESI(pos[0], pos[3]);
        nuevo_estado = cacheL1_2->calc_estadoMESI(LS, pos, MESI_otro_L1);
        cacheL1_1->actualizar_estadoMESI(nuevo_estado, pos);
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++
    cin >> num;
    contador_entradas += 1;
    }


    double miss_rate_global;
    int misses_L2;
    misses_L2 = cacheL2->obtener_misses();
    miss_rate_global = double(misses_L2)/double(contador_entradas); // Cantidad de misses total/cantidad de solicitudes.
    double miss_rate_L1_1;
    int misses_L1_1;
    misses_L1_1 = cacheL1_1->obtener_misses();
    miss_rate_L1_1= double(misses_L1_1)/double(entradas_P1); // Cantidad de misses en L1_1/cantidad de solicitudes a L1_1.
    double miss_rate_L1_2;
    int misses_L1_2;
    misses_L1_2 = cacheL1_2->obtener_misses();
    miss_rate_L1_2 = double(misses_L1_2)/double(entradas_P2); // Cantidad de misses en L1_2/cantidad de solicitudes a L1_2.
    int inv_coherencia_1;
    int inv_coherencia_2;
    inv_coherencia_1 =  cacheL1_1->obtener_inv_coherencia();
    inv_coherencia_2 =  cacheL1_2->obtener_inv_coherencia();

    cout << '\n';
    cout << "------------------------------------------------------------------" << '\n';
    cout << "Miss rate global:                          " << miss_rate_global << '\n';
    cout << "Miss rate L1 CPU1:                         " << miss_rate_L1_1 << '\n';
    cout << "Miss rate L1 CPU2:                         " << miss_rate_L1_2 << '\n';
    cout << "Invalidaciones por coherencia CPU1:        " << inv_coherencia_1 << '\n';
    cout << "Invalidaciones por coherencia CPU2:        " << inv_coherencia_2 << '\n';
    cout << "------------------------------------------------------------------" << '\n';
    cout << '\n';
    tEnd = clock();
    cout << "Tiempo de Ejecucion: " << (double)(tEnd - tStart)/CLOCKS_PER_SEC << "s" << endl;
     cout << "------------------------------------------------------------------" << '\n';
}