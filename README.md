# IE0521.Estructuras_de_computadoras_digitales_II.
Entrega de Tareas y Proyectos del Curso
Para la tarea 02, abrir la carpeta Tarea02 y leer el readme.

La entrega de la tarea 3 se realiza en conjunto con el compañero Javier Chinchilla. 
La misma se encuentra en la carpeta tarea 03, y contiene una carpeta por ejercicio de la tarea.
En las carpetas de cada ejercicio, se encuentra un archivo Readme.md con las instrucciones de 
ejecución del programa.