#include "cachemapdirec.hpp"

/*Constructor de la clase CacheMapDirec.  Especifico para estos valores 
de tag = 15 bits, index = 12 bits y offset = 5 bits. */
 CacheMapDirec::CacheMapDirec()
{

    /*Sea crea la memoria del cache y se setea en ceros.*/
    this->memoria = new int[4096];

    for(int i = 0; i < 4096; i++)
    {
        this->memoria[i] = 0; 
    }


    //Se setean los tamaños del tag, index y offset.
    this->tam = new int[3];
    this->tam[0]= 15;   //tag
    this->tam[1]= 12;   //index
    this->tam[2]= 5;    //offset

    this->misses = 0;

}

// Contructor parametrizable (respecto al tamaño del tag, index y offset).
 CacheMapDirec::CacheMapDirec(int tam_tag, int tam_index, int tam_offset)
{

    /*Sea crea la memoria del cache y se setea en ceros.*/
    int filas;
    filas = pow(2,tam_index);
    this->memoria = new int[filas];

    for(int i = 0; i < pow(2,tam_index); i++)
    {
        this->memoria[i] = 0; 
    }


    //Se setean los tamaños del tag, index y offset.
    this->tam = new int[3];
    this->tam[0]= tam_tag;   //tag
    this->tam[1]= tam_index;   //index
    this->tam[2]= tam_offset;    //offset

    this->misses = 0;

}

// Metodo destructor de la clase. Elimina los arrays creados.
CacheMapDirec::~CacheMapDirec()
{
    delete this-> memoria;
    delete this->tam;
}


void CacheMapDirec::solicitud(string dato_str)
{   
    bool hit;
    int tag_nuevo;
    int index;
    int dato;
    int mask1 = pow(2,this->tam[1]) - 1; //Mascara para aislar el index.
    int mask2 = pow(2,this->tam[0]) - 1; //Mascara para aislar el tag.
    dato = std::stoi(dato_str, nullptr, 16); //Transformo el dato en hexadecimal a decimal.
    tag_nuevo = dato >> (this->tam[1] + this->tam[2]);
    tag_nuevo = tag_nuevo & mask2;
    index = dato >> this->tam[2];
    index = index & mask1;

    /* Se compara el tag del dato nuevo con el de la columna respectiva al
    index en la memoria. */

    if (tag_nuevo == this->memoria[index]) {
        hit = true; // Si son iguales hay un hit.
    }
    else {  // Si no, se aumenta en 1 los misses para este cache, y se actualiza el tag.
        hit = false;
        this->misses = this->misses + 1;
        this->memoria[index] = tag_nuevo;
    }
}

int CacheMapDirec::obtener_misses()
{
    return this->misses;
}