El presente programa realiza el calculo de pi mediante el uso de hilos para la ejecución del cálculo.
El cálculo se realiza a través de la serie de Nilakantha.

* Para compilar el programa se requiere tener instalado el compilador g++

* Para instalarlo:

sudo apt-get install g++

* Para compilar el programa:

g++ -o calcpi calcpi.cpp -lpthread

* Para correrlo:

./calcpi <Num_threads> <Num_iteraciones>

* Parámetros:

* Num_threads: Número de hilos con los que se desea ejecutar el cálculo de pi.
* Num_iteraciones: Cantidad de términos de la serie de Nilakantha con los que se desea realizar el
cálculo de pi.

Consideraciones:
* Mas de 36 iteraciones garantizan una precisión de al menos 5 decimales.
* 739 iteraciones garantizan una precisión de 10 decimales.