#ifndef CHACELRU
#define CHACELRU

#include <iostream>
#include <bitset>
#include <string>
#include <math.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

/*Archivo cabecera con la declaración de los métodos y
* atributos de la clase CHACE_LRU (caché con política de reemplazo LRU).
*/

class CacheLRU{
public: //metodos
    CacheLRU();
    CacheLRU(int tam_tag, int tam_index, int tam_offset);
    ~CacheLRU();
    int* solicitud(string dato_str);
    int obtener_misses();
    int obtener_estadoMESI(int index, int tag); 
    // int obtener_estadoMESI(int index, int way);
    int obtener_inv_coherencia();
    // void actualizar_LRU();
    int calc_estadoMESI(short LS, int* pos, int MESI_otro_L1);
    void actualizar_estadoMESI(int nuevo_estado, int* pos);
private: //atributos
    int misses;
    int* tam;
    int** memoria;
    int inv_coherencia;
};

#endif //CHACELRU