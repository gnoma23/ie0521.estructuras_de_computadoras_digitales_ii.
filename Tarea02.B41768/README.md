Para compilar el programa ejecutar la siguiente instrucción:
g++ -o cache cache.cpp -std=c++11
Para correr el programa ejecutar la siguiente instrucción utilizando el trace art: 
gunzip -c art.trace.gz | ./cache -t # -a # -l #
Para correr el programa ejecutar la siguiente instrucción utilizando el trace mcf: 
gunzip -c mcf.trace.gz | ./cache -t # -a # -l #

Puede cambiar el orden de los parámetros. # representa el valor de cada parámetro.
a es la asociatividad, l es el tamaño del bloque o líne en bytes, y t es el tamaño 
del cache en KB.