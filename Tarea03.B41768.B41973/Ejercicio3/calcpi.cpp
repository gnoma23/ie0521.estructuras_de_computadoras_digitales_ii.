#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
using namespace std;

volatile double sum = 0;
int aux = 0;
volatile int i_actual = 0;
int iteraciones_x_thread; 
pthread_mutex_t myMutex;
 

void *mutex_calcPI(void *id){
  float sumaux = 0;
  int i_inicial = (int)((long)id*iteraciones_x_thread) ;
  //pthread_mutex_lock(&myMutex);
  //aux = i_actual;
  for(int i = i_inicial; i < (i_inicial + iteraciones_x_thread); i++)
    {
        sumaux = sumaux + (pow(-1,i))/((2*i+2)*(2*i+3)*(2*i+4));
        //i_actual += 1;
        //cout << " thread " << (long)id << ", pi actual = " ;
        //printf("%.11f\n", 3+ 4*sum);
    }

    pthread_mutex_lock(&myMutex);
    sum = sum + sumaux;
    pthread_mutex_unlock(&myMutex);  
}

int main(int argc, char **argv)
{
  clock_t tStart = clock(), tEnd;
    int num_threads = 0;
    double it_aux;
    double pi = 0;
    if (argc != 3)
    {
        cout << "Debe indicar el numero de hilos y la cantidad de iteraciones" << '\n' ;
        cout << "Mas de 36 iteraciones garantizan una precisión de 5 decimales " << '\n' ;
        cout << "739 iteraciones garantizan una precisión de 10 decimales " << '\n' ;
        abort ();
    }
    else
    {
        num_threads = atoi(argv[1]);
        it_aux = ((double)(atoi(argv[2]))/(double)num_threads);
        iteraciones_x_thread = (int)it_aux;
        cout << "Numero de threads: " << num_threads <<" iteraciones por thread (redondeando) " <<  iteraciones_x_thread << '\n' ;
        cout << '\n';
    }

    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pthread_t threads[num_threads];
    pthread_mutex_init(&myMutex,0);
    // for(long i = 0; i < 2*num_threads; i++)
    // {   
    //     if (i<num_threads) {
    //         pthread_create(&threads[i], 0, mutex_calcPI, (void*)i);
    //     }
    //     else {
    //          pthread_join(threads[i-num_threads], 0);
    //     }
    // }
    
    for(long i = 0; i < num_threads; i++)
    {
      pthread_create(&threads[i], 0, mutex_calcPI, (void*)i);
        // cout << "hola" << '\n';
    }
    for(long j = 0; j < num_threads; j++)
    {   
        pthread_join(threads[j], 0);
    }
    pthread_mutex_destroy(&myMutex);
    pi = 3.0 + 4.0*sum;
    cout << '\n';
    cout << '\n';
    cout << "El valor de pi es de: ";
    printf("%.11f\n", pi);
    cout << '\n';
    tEnd = clock();
    cout << "Tiempo de Ejecucion: " << (double)(tEnd - tStart)/CLOCKS_PER_SEC << "s" << endl;
    pthread_exit(NULL);
    
    
    return 0;
}
