#include <iostream>
#include <string>
#include <math.h>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
using namespace std;

// Declaracion de las variables globales del programa. Se trabaja con dos matrices, una que contiene los tags de cada dato, 
//  y otra que contiene los bits RRPV correspondientes a cada tag. En el caso de que la asociatividad sea mayor a dos,
//el RRPV de la matriz_RRPV[i][j], corresponde al tag de matriz_tags[i][j]. Ambas seran del mismo tamaño, y se acceden
// a traves del index. Lo mismo con las matrices matriz_tags_asocia0 y matriz_RRPV_asocia0, las cuales son empleadas cuando
// la asociatividad es menor que 2. En este caso el cache funciona con mapeo directo.
// Los ultimo cuatro enteros llevaran la cuenta de los hits y misses presentados para los loads y stores realizados.
// Se debe señalar que se va a agregar un bit al inicio al momento de guardar el tag en un bloque. Para utilizarlo como dirty bit.
int** matriz_tags;
int** matriz_RRPV;
int M;
int* matriz_tags_asocia0;
int* matriz_RRPV_asocia0;
int Load_hits = 0;
int Load_misses = 0;
int Store_hits = 0;
int Store_misses = 0;
int dirty_evictions = 0;


// Funcion encargada de calcular los tamaños, en bits, del tag, el index, y el offset de las entradas el cache.
// Recibe el tamaño del bloque o linea del cache (l), el tamaño del cache (t), y la asociatividad (a).
// Retorna en un arreglo de tres entradas, vector_tams, los tamaños mencionados.
int*  calc_tam(int l, int t, int a){
    int offset_tam;
    int index_tam;
    int tag_tam;
    offset_tam = log2 (l);
    
    if (a!=0) {
    index_tam = (log2(t) + 10) - (log2 (l) + log2(a));
    }
    else{
    index_tam = (log2(t) + 10) - log2 (l);
    }
    tag_tam = 32 - index_tam - offset_tam;

    int* vector_tams = new int[3];
    vector_tams[0] = tag_tam;
    vector_tams[1] = index_tam;
    vector_tams[2] = offset_tam;
    return vector_tams;
}

// Función encargada de crear las matrices globales. Dependiendo de la asociatividad, crea las matrices correspondientes,
// segun se especifico anteriormente.
// Retorna las filas de las matrices creadas. Las filas en realidad, se acceden con el index. 
int crear_matrices(int asociatividad, int bits_index){
    int filas;
    filas = pow(2, bits_index);
    if (asociatividad<2) {
        M = 1;
        matriz_tags_asocia0 = new int[filas];
        matriz_RRPV_asocia0 = new int[filas];

        for(int i = 0; i < filas; i++)
        {
            matriz_RRPV_asocia0[i] = 1;
        }
        
    }
    else
    {   
        M = 2;
        matriz_tags = new int*[filas];
            for(int i = 0; i < filas; i++)
            {
                matriz_tags[i] = new int[asociatividad];
            }
        matriz_RRPV = new int*[filas];
            for(int i = 0; i < filas; i++)
            {
                matriz_RRPV[i] = new int[asociatividad];
            }

        
        for(int i = 0; i < filas; i++)
        {   
            for(int j = 0; j < asociatividad; j++)
            {
                matriz_RRPV[i][j]= 3;
                matriz_tags[i][j]= 0;
            }   
        }
        
    }
return filas;
}

// Esta funcion es la encargada de de actualizar la matriz de RRPV correspondiente, cuando hay un hit sobre un tag de la matriz de 
// tags. Recibe la asociatividad para saber cual matriz de RRPV debe actualizar, un vector de dos posiciones que contiene la posicion
// de la matriz de tags sobre la cual se hizo el hit, y actualiza con un cero, en esa misma posicion, pero en la matriz de RRPV.
void actualizar_RRPV_hit(int LS, int asociatividad, int* pos_hit){   
    int tag_drty_bit = 0;
    if (asociatividad >=2) {
        
        
        if (LS == 1) { // Cuando hay un hit por store, es necesario actualizar con un 1 el dirty bit, en el caso de que este este en 0.
                    
            tag_drty_bit = matriz_tags[pos_hit[0]][pos_hit[1]] & 1; // Se extrae el dirty bit. 
            
            if (tag_drty_bit == 0) { // Si es cero, se actualiza el dirty bit con un 1.
                matriz_tags[pos_hit[0]][pos_hit[1]] += 1;
            }
            
        }

        matriz_RRPV[pos_hit[0]][pos_hit[1]] = 0;

    }else{
        
        if (LS == 1) { // Cuando hay un hit por store, es necesario actualizar con un 1 el dirty bit, en el caso de que este este en 0.
                        
            tag_drty_bit = matriz_tags_asocia0[pos_hit[0]] & 1;

            if (tag_drty_bit == 0) { //Se el dirty bit es 0, se actualiza con 1 por ser un store (la instruccion).
                matriz_tags_asocia0[pos_hit[0]] += 1; 
            }

        }

        matriz_RRPV_asocia0[pos_hit[0]] = 0;
    }
    
}  
// Cuando hay un miss, esta funcion es la encargada de actualizar las matrices de tags, y de RRPV. Como se debe buscar en la matriz RRPV,
// cual de las posiciones contiene un 2^M - 1, esta funcion recibe el index del dato, que corresponde a la fila de la matriz de tags sobre
// la cual ocurrio el miss. Asi mismo, tambien corresponde a fila de la matriz RRPV donde se debe buscar cual dato se debe eliminar de la cache.
// Recibe la asociatividad para recorrer las columnas de la matriz RRPV buscando un 2^M -1, cuando lo encuentra en la posicion matriz_RRPV[i][j],
// introduce el nuevo tag, nuevo_tag, que recibe en matriz_tags[i][j]. Luego de introducir el tag, actualiza matriz_RRPV[i][j] con un 2^M - 2.
// La logica de cuando la asociatividad es menor a dos es la misma, solo que en este caso ambas matrices son de la sola columna, e introduce
// el nuevo tag en la fila respectiva al index del dato donde se produjo el miss. 
void actualizar_RRPV_miss(int LS, int index, int asociatividad, int nuevo_tag){
    int sumar_1 = 1;
    int mask3 = 1;
    int tag_viejo_drty;
    // int tag_nuevo_drty;
    if (asociatividad >= 2) {
        
        for(int j = 0; j < asociatividad; j++)
        {
            if (matriz_RRPV[index][j] == pow(2,M)-1) {

                tag_viejo_drty = matriz_tags[index][j] & mask3; //Se extrae el dirty bit del dato que voy a sacar del cache.
                if (tag_viejo_drty == 1) { // Si el dirty bit es 1, aumento el contador de dirty evictions.
                    dirty_evictions += 1;
                }
                
                if (LS == 1) { //Si la nueva instruccion es un store, pongo en 1 el dirty bit que voy a gaurdar.
                    nuevo_tag = nuevo_tag << 1;
                    nuevo_tag += 1;
                }
                else { // Si esun load, lo pongo en cero.
                    nuevo_tag = nuevo_tag << 1;
                }
                matriz_tags[index][j] = nuevo_tag;
                matriz_RRPV[index][j] = pow(2,M)-2; // Como inserto un nuevo dato, actualizo su bit RRPV a 2.
                sumar_1 = 0;
                break;
            }   
        }
        if (sumar_1 == 1) {
           for(int i = 0; i < asociatividad; i++)
           {
                matriz_RRPV[index][i] += 1;
           }
        actualizar_RRPV_miss(LS, index, asociatividad, nuevo_tag);
        }
    }
    else
    {   
        tag_viejo_drty = matriz_tags_asocia0[index] & mask3;
        if (tag_viejo_drty == 1) {
            dirty_evictions += 1;
        }
        if (LS == 1) {
            nuevo_tag = nuevo_tag << 1;
            nuevo_tag += 1;
        }
        else {
            nuevo_tag = nuevo_tag << 1;
        }
        matriz_tags_asocia0[index] = nuevo_tag;
        matriz_RRPV_asocia0[index] =  pow(2,M)-2;
    } 
}

// Esta es la funcion encargada de llevar cabo la simulacion del cache. Desde el main, esta funcion recibe LS, el dato, que viene
// como una string, un vector que contiene los tamaños en bits del tag, index y offset, correspondientes al dato, y la asociatividad.
// El entero LS contiene la informacion sobre si lo que se quiere hacer es un store, o un load del dato. En dado caso de que se de un hit,
// o un miss, se aumentara en 1, el contador correspondiente. (Store_misses, Store_loads, Load_misses o Load _hits).
// Con la string dato_str, lo que se obtiene es el dato de 32 bits en hexadecimal que viene por la linea de datos al cache. En la funcion,
// esta string se transforma en un entero, y se le aplican mascaras para aislar el tag y el index correspondientes al dato.
// Se toma el index para encontrar la fila de la matriz de tags sobre la cual tiene que buscarse el dato, y se recorre dicha fila, y
// si se encuentra en alguna de las columnas un tag que corresponda al tag del nuevo dato que esta entrando, se obtiene un hit, y se llama 
// a la funcion actualizar_RRPV_hit con la posicion sobre la cual se hizo el hit.
// En caso de no encontrarse el tag en la columna, se llama a la funcion actualizar_RRPV_miss con la fila (index) en cual hubo un miss. 
// Como se explica en los comentarios de dicha funcion, se actualizan las matrices.
// En el caso de que la asociatividad sea menor que dos, se simplifica, puesto que solo tiene comparar el tag con la fila respectiva al index.
void funcion_cache(int LS, string dato_str,int* tam, int asociatividad){
    int tag_nuevo;
    int index;
    int dato;
    int mask1 = pow(2,tam[1]) - 1;
    int mask2 = pow(2,tam[0]) - 1;
        dato = std::stoi(dato_str, nullptr, 16);
        tag_nuevo = dato >> (tam[1] + tam[2]);
        tag_nuevo = tag_nuevo & mask2;
        index = dato >> tam[2];
        index = index & mask1;
        int hit = 0;
        int* posicion_tags = new int[2];
        // int tag_nuevo;
        int tag_anterior;
        
        if (asociatividad >= 2) {
            for(int j = 0; j < asociatividad; j++)
            {   
                tag_anterior = matriz_tags[index][j] >> 1;
                tag_anterior = tag_anterior & mask2;
                if (tag_anterior == tag_nuevo) {
                    hit = 1;
                    posicion_tags[0] = index;
                    posicion_tags[1] = j;
                    break;
                }
            }
        }
        else
        {   
            tag_anterior = matriz_tags_asocia0[index] >> 1;
            tag_anterior = tag_anterior & mask2;
            if (tag_anterior == tag_nuevo) {
                hit = 1;
                posicion_tags[0] = index;
                posicion_tags[1] = 0;
            } 
        }
        if (hit == 1) {
            if (LS == 0) {
                Load_hits += 1;
            }
            else
            {
                Store_hits += 1;
            } 
            actualizar_RRPV_hit(LS, asociatividad, posicion_tags);
        }
        else
        {
        if (LS == 0) {
            Load_misses += 1;
        }
        else
        {
            Store_misses += 1;
        }
            actualizar_RRPV_miss(LS, index, asociatividad, tag_nuevo);
        }
}
// Funcion main. Sobre ella se controla el flujo del programa.
// Inicialmente, mediante la funcion getopt de C, se obtienen los tamanos, pasados por consola, del cache, el bloque, y la asociatividad con 
// la que se va a crear el cache. Por la forma en que esta implementada, esta funcion permite pasar los parametros en desorden, ademas de detener
// el programa cuando haga falta alguno de los tres.
// Se llama a las funciones calc_tam() con los tamaños obtenidos, y a crear_matrices() con el index, obtenido de calc_tam(), y la asociatividad.
// Despues de que se tienen las matrices creadas, se comienza a leer los datos que son entradas, a traves de la funcion cin.
// Se va leyendo fila por fila del archivo con las entradas, se guarda el LS y el dato, y con estos se llama a la funcion_chace() para que 
// verifique si con este dato nuevo existira un miss o un hit.
// Se lee el archivo hasta que termina.
// Por ultimo, se imprimen los resultados, y se eliminan las matrices creadas con memoria dinamica.
int main (int argc, char **argv)
{   

    int aflag = 0; // Banderas. Para determinar si se estan pasando todos los parametros necesarios.
    int tflag = 0;
    int lflag = 0;
    int a = 0; // Asociatividad. 
    int t = 0;
    int l = 0;
    int c;
    while ((c = getopt (argc, argv, "a:t:l:")) != -1){
        switch (c)
        {
        case 'a':
        aflag = 1;
        a = atoi(optarg);
        break;
        case 't':
        tflag = 1;
        t = atoi(optarg);
        break;
        case 'l':
        lflag = 1;
        l = atoi(optarg);
        break;
        return 1;
        default:
        cout << "Debe introducir bien  las banderas y el tamaño correspondiente, en el orden que desee." << '\n';
        cout << "Por ejemplo:  -t <#> -l <#> -a <#>. Se puede cambiar el orden." << '\n';
        abort ();
        }        
    }
    int flags = aflag + tflag + lflag;
    if (flags != 3) {
        cout << "Hizo falta un tamaño." << '\n';
        cout << "Por ejemplo:  -t <#> -l <#> -a <#>. Se puede cambiar el orden." << '\n';
        abort(); 
    }

    int *tamanos;
    tamanos = calc_tam(l, t, a);
    int filas = crear_matrices(a, tamanos[1]);

    char num;
    string dato;
    int LS;
    int ignorar;
    int contador_entradas = 0;
    cin >> num;
    while(!cin.eof() & num!='\n'){
        contador_entradas += 1;
        cin >> LS;
        cin >> dato;
        cin >> ignorar;
        funcion_cache(LS, dato, tamanos, a);
        cin >> num;
    }
    int hits_totales = 0;
    float misses_totales = 0;
    float read_miss_rate = 0;
    float overal_miss_rate = 0;
    misses_totales = (Load_misses + Store_misses);
    hits_totales = Load_hits + Store_hits;
    read_miss_rate = (float)Load_misses/(float)(contador_entradas);
    overal_miss_rate = (float)misses_totales/(float)(contador_entradas);

    // Imprimir los resultados del programa.
    cout << "_____________________________" << '\n';
    cout << "Cache parameters" << '\n';
    cout << "-----------------------------" << '\n';
    cout << "Cache Size (KB): " << t << '\n';
    cout << "Cache Associativity: " << a << '\n';
    cout << "Cache Block Size (bytes): " << l << '\n';
    cout << "-----------------------------" << '\n';
    cout << "Overal Miss Rate: " << overal_miss_rate << '\n';
    cout << "Read miss rate: " << read_miss_rate << '\n';
    cout << "Dirty evictions: " << dirty_evictions << '\n'; 
    cout << "Load misses: " << Load_misses << "\n";
    cout << "Store misses: " << Store_misses << "\n";
    cout << "Total misses: " << misses_totales << "\n";
    cout << "Load hits: " << Load_hits << "\n";
    cout << "Store hits: " << Store_hits << "\n";
    cout << "Total hits: " << hits_totales << "\n";
    cout << "-----------------------------" << '\n';

    if (a<2) {
        delete matriz_tags_asocia0;
    }
    else{
        delete[] matriz_tags;
        delete[] matriz_RRPV;
    }
    
  
    return 0;

}

 