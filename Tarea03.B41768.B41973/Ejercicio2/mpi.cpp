#include <mpi.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <time.h>

using namespace std;

int main (int argc, char** argv){
  MPI_Status status;
  MPI_Request request;
  int rank, size, proc = stoi(argv[2]) , cont = 0, rev = 1, sizem = stoi(argv[1]), aux = 0, pos;
  int A[sizem][sizem], V[sizem], C[sizem], AUX1[sizem] , AUX2[sizem];
  bool flag = true;
  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); //Se Obtiene el rango del proceso
  MPI_Comm_size(MPI_COMM_WORLD, &size); //Se obtiene tama\~no del mundo
  //cout << "W_rank: "<< rank << "\tW_size: "<< size << endl;
  if (rank == 0){ //Proceso maestro
    clock_t tStart = clock(), tEnd;
    for(int i = 0; i < sizem; i++){ // Se inicializa la matriz y los vectores segun el tama\~no ingresado
      for(int j = 0; j < sizem; j++){
	A[i][j] = i + j;
      }
      V[i] = i;
      C[i] = 0;
    }
    for (int i = 1; i <= proc && i < sizem ; i++){//Se envian primeran instruciones
      //cout << "Sent " << cont << endl; 
      MPI_Send(&cont, 1, MPI_INT, i, 0, MPI_COMM_WORLD); //Fila a trabajar
      MPI_Send(&A[cont], sizem, MPI_INT, i, 0, MPI_COMM_WORLD); //Valores de la fila
      MPI_Send(&V, sizem, MPI_INT, i, 0, MPI_COMM_WORLD);// Vector
      cont++;
    }
    while (flag){
      
      MPI_Probe(MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status); //Observa el canal de comunicacion para esperar el ingreso de datos
      MPI_Recv(&pos, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); //Se recibe fila
      MPI_Recv(&C[pos], 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);// Se recibe resultado
      
      //cout <<"\tLlego: " << pos << " de :" << status.MPI_SOURCE  << " Rev " << rev << endl;

      rev++;
      if (cont < sizem){ //Se comprueba si aun existen datos por enviar
	MPI_Send(&cont, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
	MPI_Send(&A[cont], sizem, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
	MPI_Send(&V, sizem, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD);     
	//cout << "Sent " << status.MPI_SOURCE << " cont " << cont << endl;
	cont++;
      }else if (rev > sizem && cont == sizem){// Se comprueba si se recibieron todos los datos
	flag = false;
      }
      
    }
    for (int i = 1; i < size; i++){ // Se envian se\~nales de stop a los esclavos
      //cout << "END" << i << endl;

      MPI_Isend(&aux, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &request);
    }
    
    cout << endl << "Matriz" << endl; //Se imprime matrix
    for(int i = 0; i < sizem; i++){
      for(int j = 0; j < sizem; j++){
	cout << A[i][j] << "\t" ;
      }
      cout << endl;
    }
    cout << endl << "Vector" << endl; // Se imprime vector
    for(int i = 0; i < sizem; i++){
      cout << V[i] << endl;
    }
    cout << endl << "Resultado" << endl; //Se imprime resultado
    for(int i = 0; i < sizem; i++){
      cout << C[i] << endl;
    }
    //MPI_Recv(&respo, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD,  MPI_STATUS_IGNORE);
    //MPI_Recv(&respo, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD,  MPI_STATUS_IGNORE);
    tEnd = clock();
    cout << "Tiempo de Ejecucion: " << (double)(tEnd - tStart)/CLOCKS_PER_SEC << "s" << endl;  
  }else{
    while (flag){
      MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);// Se espera comandos del maestro
      
      if (status.MPI_TAG == 0){//Se procesa request
	MPI_Recv(&pos, 1, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD,  MPI_STATUS_IGNORE); //Se recibe numero de fila
	MPI_Recv(&AUX1, sizem, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); //Se recibe los datos de la fila
	MPI_Recv(&AUX2, sizem, MPI_INT, status.MPI_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);// Se recibe los datos del vector

	aux = 0;
	for(int i = 0; i < sizem; i++){//Se realiza la multiplicacion.
	  aux = aux + AUX1[i]*AUX2[i];
	  
	}
	
	//	cout << rank << ". DONE: " << pos << " RES: "<< aux <<  endl;
	MPI_Send(&pos, 1, MPI_INT, 0, 0, MPI_COMM_WORLD); //Se retorna la posicion de la fila
	MPI_Send(&aux, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);// Se retorna el resultado de la operacion.
	//cout << "DONE: " << rank  << "dato: "<< number <<  endl;

      }else if (status.MPI_TAG == 1){//Se recibe el mensaje de parada por parte del maestro.
	MPI_Irecv(&pos, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &request);
	flag = false;
	
      }
    }
    //cout << "/tEND : " << rank << endl;
  }
  
  MPI_Finalize();
  return 0;
}
//mpiCC mpi.cpp -o out && mpirun --hostfile hostfile -np 4 ./out
