#include "cacheLRU.hpp"

/* Constructor de la clase CacheLRU. Especifico para estos valores 
de tag = 19 bits, index = 8 bits y offset = 5 bits. */
 CacheLRU::CacheLRU() 
{
    this->misses = 0;
    this->inv_coherencia = 0;
    
    this->tam = new int[3];
    this->tam[0] = 19; //tag
    this->tam[1] = 8;  //index
    this->tam[2] = 5; //offset

    /*Se genera la matriz de memoria. La primera columna va a contener
    el tag y el bit de LRU del dato de way0, la segunda columna, el tag
    y bit LRU del way 1. La tercera columna el estado MESI del way0, y
    la cuarta columna, el estado MESI del way1.
    Un LRU 1 en la via significa el el dato en esa via es el mas recientemente
    usado. Un cero, indicaque el dato en esa via es el que debe victimizarse*/
    int filas;
    filas = pow(2,8);
    this->memoria = new int*[filas];
    for(int i = 0; i < pow(2,8); i++)
    {
        this->memoria[i] = new int[4];
    }
    for(int i = 0; i < pow(2,8); i++)
    { 
        for(int j = 0; j < 4; j++)
        {
             this->memoria[i][j] = 0;
        }
    }
}

// Contructor parametrizable (respecto al tamaño del tag, index y offset).
 CacheLRU::CacheLRU(int tam_tag, int tam_index, int tam_offset)
{
    this->misses = 0;
    this->inv_coherencia = 0;

    this->tam = new int[3];
    this->tam[0] = tam_tag; //tag
    this->tam[1] = tam_index;  //index
    this->tam[2] = tam_offset; //offset

    /*Se genera la matriz de memoria. La primera columna va a contener
    el tag y el bit de LRU del dato de way0, la segunda columna, el tag
    y bit LRU del way 1. La tercera columna el estado MESI del way0, y
    la cuarta columna, el estado MESI del way1.
    Un LRU 1 en la via significa el el dato en esa viaes el mas recientemente
    usado. Un cero, indicaque el dato en esa via es el que debe victimizarse*/
    int filas;
    filas = pow(2,this->tam[1]);
    this->memoria = new int*[filas];

    for(int i = 0; i < pow(2,this->tam[1]); i++)
    {
         this->memoria[i] = new int[4];
    }

    for(int i = 0; i < pow(2,this->tam[1]); i++)
    { 
        for(int j = 0; j < 4; j++)
        {
             this->memoria[i][j] = 0;
        }
    }
    
}

int CacheLRU::obtener_misses()
{
    return this->misses;
}

/*Este metodo recibe una solicitud de un dato, descompone el mismo en en el index, el tag y el offset.
Se encarga de revisar si hay un miss o un hit para el dato entrante. Actualiza el estado LRU de la vía
correspodiente el index. Se encarga también de victimizar el dato respectivo cuando hay un hit. Aplicando
la política de reemplazo LRU.
Si hay un hit, retorna un array con la via y el index del dato. Además de un 1 indicando que en esa posición
se dio un hit.
Si hay un miss, retorna un array con el index y un 0 indicando que hubo un miss en  esa linea..*/
int* CacheLRU::solicitud(string dato_str)
{   
    int hit;
    int tag_nuevo;
    int tag_actual;
    int index;
    short LRU0;
    short LRU1;
    int dato;
    int mask1 = pow(2,this->tam[1]) - 1; //Mascara para aislar el index.
    int mask2 = pow(2,this->tam[0]) - 1; //Mascara para aislar el tag.
    dato = std::stoi(dato_str, nullptr, 16); //Transformo el dato en hexadecimal a decimal.
    tag_nuevo = dato >> (this->tam[1] + this->tam[2]);
    tag_nuevo = tag_nuevo & mask2;
    index = dato >> this->tam[2];
    index = index & mask1;

    /* Se compara el tag del dato entrante con el tag de ambas vias. Si hay un hit, se retorna el index,
    la via y un 1 (hit), en el array pos.*/
    int* pos = new int[4];
    hit = 0;
    for(int i = 0; i < 2; i++) 
    {   
        tag_actual = this->memoria[index][i];
        tag_actual = tag_actual >> 1;
        
        if (tag_actual == tag_nuevo) { 
            hit = 1;
            pos[0] = index;
            pos[1] = i;
            pos[2] = 1;
            pos[3] = tag_actual; //Restorno el tag sobre el que se dio el hit.
        }
        
    }
    /*Cuando hay un hit, se pregunta si la via en la que se dio el LRU es 1 o 0. Se actualiza el estado
    LRU de dicha via y de la otra.*/

    if (hit == 1) 
    {
        if (pos[1]==0) 
        {
            LRU1 = 0;
            LRU0 = 1;
        }
        else
        {
            LRU1 = 1;
            LRU0 = 0;
        }

        if ((this->memoria[index][LRU1]&1)== 0) {
            this->memoria[index][LRU1] = this->memoria[index][LRU1] + 1;
        }
        if ((this->memoria[index][LRU0]&1) == 1) {
            this->memoria[index][LRU0] = this->memoria[index][LRU0] - 1;
        }
    }
    else 
    {   
        /* En caso de darse un miss, se aumenta el contador de misses. */
        this->misses = this->misses + 1;
        pos[2] = 0;
        LRU0 = this->memoria[index][0] & 1; // Se calcula el estado LRU de la via 0 respectiva al index.
        tag_nuevo = tag_nuevo << 1;
        tag_nuevo += 1; // El nuevo dato se actualiza con un LRU = 1.
        pos[3] = tag_nuevo; // Retorno el tag que voy a escribir. 
        if (LRU0 == 0) { //Si la via cero tiene un LRU = 0, se victimiza el dato que esta en esta, y se introduce el nuevo.
            pos[1] = 0;
            this->memoria[index][0] = tag_nuevo;
            if (this->memoria[index][1]&1>0){
                this->memoria[index][1] = this->memoria[index][1] - 1; 
            }
        }
        else { //Si la via cero tiene un LRU = 1, se victimiza el dato que esta en esta, y se introduce el nuevo.
            pos[1] = 1;
            this->memoria[index][1] = tag_nuevo;
            
            if (this->memoria[index][0]&1>0){
                this->memoria[index][0] = this->memoria[index][0] - 1;
            }
        }
        
    }
    return pos;
}

/*Este metodo se encarga de obtener el estado MESI del cache. Primero busca si el dato respectivo al 
index y al tag que recibe esta en su memoria. Si no esta retorna un 4. Si esta, retorna el estado MESI 
respectivo al dato. */
int CacheLRU::obtener_estadoMESI(int index, int tag)
{
    int retorno;
    int tag_otroL1;
    int tag_esteL1;
    int mismo_dato;
    int via;
    mismo_dato = 0;
    tag_otroL1 = tag >> 1;
    
    for(int i = 0; i < 2; i++)
    {
     tag_esteL1 = this->memoria[index][i];
     tag_esteL1 = tag_esteL1 >> 1;
     
     if (tag_esteL1 == tag_otroL1) {
        mismo_dato = 1;
        via = i;
        break;
     }
     
    }

    if (mismo_dato == 1) 
    {
        retorno = this->memoria[index][via+2];
    }
    else 
    {
        retorno = 4;
    }
    return retorno;
}

int CacheLRU::obtener_inv_coherencia()
{
    return this->inv_coherencia;
}

/* Este metodo se encarga de actualizar el estado MESI del cache actual, y retornar el estado MESI al que
se debe actualizar el otro cache. Primero consulta si en el otro cache estaba el dato al cual se le quiere 
actualizar el estado (MESI_otro_L1 ==4?), si no esta, el dato en este cache pasa a exclusivo, y el otro cache 
no se toca. Si el dato si esta, se actuliza el estado de este cache dependiendo de si se quiere hacer una lectura
o una escritura del dato, y dependiendo del estado MESI del dato en el otro cache. Tambien retorna un 4 para
indicar que no se deben modificar los estados del otro cache, o bien el estado nuevo al que este se debe actualizar.*/
int CacheLRU::calc_estadoMESI(short LS, int* pos, int MESI_otro_L1)
{
    int nuevo_MESI_otro_L1;
    int retorno;

    
    if (MESI_otro_L1 == 4) { // Si es cuatro significa que el dato no está en el otro caché.
        this->memoria[pos[0]][pos[1]+2] = 1;
        retorno = 4;
    }
    else{
        if (pos[2] == 0) {
            this->memoria[pos[0]][pos[1]+2] = 1;
             nuevo_MESI_otro_L1 = 3;
        }
        else
        {
            if (LS == 1) {
                this->memoria[pos[0]][pos[1]+2] = 0;
                nuevo_MESI_otro_L1 = 3;

            }
            else {
                if (MESI_otro_L1 == 3) {
                    this->memoria[pos[0]][pos[1]+2] = 1;
                    nuevo_MESI_otro_L1  = 3;
                }                
                else {
                    this->memoria[pos[0]][pos[1]+2] = 2;
                    nuevo_MESI_otro_L1  = 2;
                }
            }
        }
        retorno =  nuevo_MESI_otro_L1;
    }
     
   return retorno;
}
/* Este metodo es el encargado de actualizar el estado MESI del cache actual a partir de lo que el otro
cache le indica. */
void CacheLRU::actualizar_estadoMESI(int nuevo_estado, int* pos)
{   
    
    if (nuevo_estado != 4) {
        if (nuevo_estado == 3 && this->memoria[pos[0]][pos[1]+2] != 3) {
            this->inv_coherencia += 1;
        }
        this->memoria[pos[0]][pos[1]+2] = nuevo_estado;
    }
}