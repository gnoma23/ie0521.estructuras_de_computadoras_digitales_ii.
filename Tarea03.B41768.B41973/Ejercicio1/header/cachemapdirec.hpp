#ifndef CHACEMAPDIREC
#define CHACEMAPDIREC

#include <iostream>
#include <string>
#include <math.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
using namespace std;


/*Archivo cabecera con la declaración de los métodos y
* atributos de la clase cacheMapDirec (caché con mapeo directo).
*/

class CacheMapDirec{
public: //metodos
    CacheMapDirec();
    CacheMapDirec(int tam_tag, int tam_index, int tam_offset);
    ~CacheMapDirec();
    void solicitud(string dato_str);
    int obtener_misses();
private: //atributos
    int misses;
    int* tam;
    int* memoria;
};



#endif //CACHEMAPDIREC